package arbolesbinarios;

import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JPanel;
/**
 *
 * @author AndresFWilT
 */
public class GraficaArbolBinario extends JPanel {

    private final ArbolBinario miarbol;
    private char menor;
    public static final int DIAMETRO = 40;
    public static final int RADIO = DIAMETRO / 2;
    public static final int ANCHO = 50;
    private String dato;

    public GraficaArbolBinario(ArbolBinario miarbol, char menor,String dato) {
        this.miarbol = miarbol;
        this.menor = menor;
        this.dato=dato;
        repaint();
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        pintar(g, getWidth() / 2, 20, miarbol.getRaiz());
    }

    private void pintar(Graphics g, int x, int y, Nodo r) {
        if (r == null) {
            return;
        }
        if (menor == r.getDato()) {
            System.out.println(menor+" "+ r.getDato());
            g.setColor(Color.RED);
        } else {
            g.setColor(Color.BLACK);
        }
     
        int EXTRA = new ArbolBinario().nodosCompletos(r) * (ANCHO / 2);
        if (menor == r.getDato()) {
            System.out.println(menor+" "+ r.getDato());
            g.setColor(Color.RED);
            g.drawString(dato , x + 10, y + 20);
        } else {
            g.setColor(Color.BLACK);
            g.drawString(Character.toString(r.getDato()) +","+r.getValor(), x + 10, y + 20);
        }
        
        g.setColor(Color.BLACK);
        if (r.getIzquierdo() != null) {
            g.drawLine(x + RADIO - 5, y + RADIO + 15, x - ANCHO - EXTRA + RADIO, y - 15 + ANCHO + RADIO);
        }
        if (r.getDerecho() != null) {
            g.drawLine(x + RADIO - 5, y + RADIO + 15, x + ANCHO + EXTRA + RADIO, y - 15 + ANCHO + RADIO);
        }
        pintar(g, x - ANCHO - EXTRA, y + ANCHO, r.getIzquierdo());
        pintar(g, x + ANCHO + EXTRA, y + ANCHO, r.getDerecho());
    }
}

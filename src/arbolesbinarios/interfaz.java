package arbolesbinarios;

/**
 *
 * @author AndresFWilT
 */
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

public class interfaz extends JFrame implements ActionListener {

    private JPanel panel;
    JScrollPane scroll;
    JLabel labelA, labelB, labelC, labelD, labelE, labelF, labelG,
            labelH, labelI, labelJ, labelK, labelL, labelM, labelN,
            labelO, labelP, labelQ, labelR, labelS, labelT, labelU,
            labelV, labelW, labelX, labelY, labelZ,labelMin;
    JButton bEnviar;
    JTextField txt[][];
    String matriz[][];
    char letras[][];
    int valor[][];

    public interfaz() {
        this.setSize(650, 600);
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setTitle("Arbol N-ario a binario");
        letras = new char[26][9];
        valor = new int[26][9];
        matriz = new String[26][9];
        iniciarComponentes();
    }

    public void iniciarComponentes() {
        scroll = new JScrollPane();
        scroll.setBounds(5, 10, 380, 150);
        panel = new JPanel();
        panel.setLayout(null);
        panel.setBackground(Color.WHITE);
        bEnviar = new JButton("enviar");
        bEnviar.setBounds(450, 500, 100, 15);
        bEnviar.addActionListener(this);
        labelA = new JLabel("A");
        labelA.setBounds(10, 30, 10, 10);
        labelB = new JLabel("B");
        labelB.setBounds(10, 50, 10, 10);
        labelC = new JLabel("C");
        labelC.setBounds(10, 70, 10, 10);
        labelD = new JLabel("D");
        labelD.setBounds(10, 90, 10, 10);
        labelMin = new JLabel("distancia mas corta");
        labelMin.setBounds(400, 10, 200, 20);
        panel.add(labelMin);
        labelE = new JLabel("E");
        labelE.setBounds(10, 110, 10, 10);
        labelF = new JLabel("F");
        labelF.setBounds(10, 130, 10, 10);
        labelG = new JLabel("G");
        labelG.setBounds(10, 150, 10, 10);
        labelH = new JLabel("H");
        labelH.setBounds(10, 170, 10, 10);
        labelI = new JLabel("I");
        labelI.setBounds(10, 190, 10, 10);
        labelJ = new JLabel("J");
        labelJ.setBounds(10, 210, 10, 10);
        labelK = new JLabel("K");
        labelK.setBounds(10, 230, 10, 10);
        labelL = new JLabel("L");
        labelL.setBounds(10, 250, 10, 10);
        labelM = new JLabel("M");
        labelM.setBounds(10, 270, 10, 10);
        labelN = new JLabel("N");
        labelN.setBounds(10, 290, 10, 10);
        labelO = new JLabel("O");
        labelO.setBounds(10, 310, 10, 10);
        labelP = new JLabel("P");
        labelP.setBounds(10, 330, 10, 10);
        labelQ = new JLabel("Q");
        labelQ.setBounds(10, 350, 10, 10);
        labelR = new JLabel("R");
        labelR.setBounds(10, 370, 10, 10);
        labelS = new JLabel("S");
        labelS.setBounds(10, 390, 10, 10);
        labelT = new JLabel("T");
        labelT.setBounds(10, 410, 10, 10);
        labelU = new JLabel("U");
        labelU.setBounds(10, 430, 10, 10);
        labelV = new JLabel("V");
        labelV.setBounds(10, 450, 10, 10);
        labelW = new JLabel("W");
        labelW.setBounds(10, 470, 12, 10);
        labelX = new JLabel("X");
        labelX.setBounds(10, 490, 10, 10);
        labelY = new JLabel("Y");
        labelY.setBounds(10, 510, 10, 10);
        labelZ = new JLabel("Z");
        labelZ.setBounds(10, 530, 10, 10);
        txt = new JTextField[26][9];
        int x = 25;
        int y = 30;
        for (int i = 0; i < 26; i++) {
            x = 25;
            for (int j = 0; j < 9; j++) {
                txt[i][j] = new JTextField();
                txt[i][j].setBounds(x, y, 30, 15);
                panel.add(txt[i][j]);
                x = x + 35;
            }
            y = y + 20;
        }
        panel.add(bEnviar);
        panel.add(labelA);
        panel.add(labelB);
        panel.add(labelC);
        panel.add(labelD);
        panel.add(labelE);
        panel.add(labelF);
        panel.add(labelG);
        panel.add(labelH);
        panel.add(labelI);
        panel.add(labelJ);
        panel.add(labelK);
        panel.add(labelL);
        panel.add(labelM);
        panel.add(labelN);
        panel.add(labelO);
        panel.add(labelP);
        panel.add(labelQ);
        panel.add(labelR);
        panel.add(labelS);
        panel.add(labelT);
        panel.add(labelU);
        panel.add(labelV);
        panel.add(labelW);
        panel.add(labelX);
        panel.add(labelY);
        panel.add(labelZ);
        panel.setPreferredSize(new Dimension(200, 200));
        scroll.setViewportView(panel);
        add(scroll);
    }

    public void enviar() {
        ArbolBinario a1 = new ArbolBinario();
        
        a1.Añadir('a', 0, ' ', 1);
        String entra1;
        String entra2;
        String[] parts1;
        String[] parts2;
        String part1;
        String part2;
        String part3;
        String part4;
        for (int i = 0; i < 26; i++) {
            for (int j = 0; j < 9; j++) {
                letras[i][j] = ' ';
                valor[i][j] = 0;
            }
        }
        
        for (int i = 0; i < 26; i++) {
            for (int j = 0; j < 9; j++) {
                matriz[i][j] = txt[i][j].getText();
                entra1 = txt[i][j].getText();
                if (!entra1.isEmpty()) {
                    parts1 = entra1.split(",");
                    part1 = parts1[0];
                    letras[i][j] = part1.charAt(0);
                    part2 = parts1[1];
                    valor[i][j] = Integer.parseInt(part2);
                    if (i == 0) {
                        if (j == 0) {
                            a1.Añadir(part1.charAt(0), Integer.parseInt(part2), 'a', 1);
                        } else {
                            entra2 = txt[i][j - 1].getText();
                            parts2 = entra2.split(",");
                            part3 = parts2[0];
                            part4 = parts2[1];
                            a1.Añadir(part1.charAt(0), Integer.parseInt(part2), part3.charAt(0), 0);
                        }

                    } else if (i == 1) {
                        if (j == 0) {
                            a1.Añadir(part1.charAt(0), Integer.parseInt(part2), 'b', 1);
                        } else {
                            entra2 = txt[i][j - 1].getText();
                            parts2 = entra2.split(",");
                            part3 = parts2[0];
                            part4 = parts2[1];
                            a1.Añadir(part1.charAt(0), Integer.parseInt(part2), part3.charAt(0), 0);
                        }

                    } else if (i == 2) {
                        System.out.println("Si entro");
                        if (j == 0) {
                            a1.Añadir(part1.charAt(0), Integer.parseInt(part2), 'c', 1);
                        } else {
                            entra2 = txt[i][j - 1].getText();
                            parts2 = entra2.split(",");
                            part3 = parts2[0];
                            part4 = parts2[1];
                            a1.Añadir(part1.charAt(0), Integer.parseInt(part2), part3.charAt(0), 0);
                        }

                    } else if (i == 3) {
                        if (j == 0) {
                            a1.Añadir(part1.charAt(0), Integer.parseInt(part2), 'd', 1);
                        } else {
                            entra2 = txt[i][j - 1].getText();
                            parts2 = entra2.split(",");
                            part3 = parts2[0];
                            part4 = parts2[1];
                            a1.Añadir(part1.charAt(0), Integer.parseInt(part2), part3.charAt(0), 0);
                        }

                    } else if (i == 4) {
                        if (j == 0) {
                            a1.Añadir(part1.charAt(0), Integer.parseInt(part2), 'e', 1);
                        } else {
                            entra2 = txt[i][j - 1].getText();
                            parts2 = entra2.split(",");
                            part3 = parts2[0];
                            part4 = parts2[1];
                            a1.Añadir(part1.charAt(0), Integer.parseInt(part2), part3.charAt(0), 0);
                        }

                    } else if (i == 5) {
                        if (j == 0) {
                            a1.Añadir(part1.charAt(0), Integer.parseInt(part2), 'f', 1);
                        } else {
                            entra2 = txt[i][j - 1].getText();
                            parts2 = entra2.split(",");
                            part3 = parts2[0];
                            part4 = parts2[1];
                            a1.Añadir(part1.charAt(0), Integer.parseInt(part2), part3.charAt(0), 0);
                        }

                    } else if (i == 6) {
                        if (j == 0) {
                            a1.Añadir(part1.charAt(0), Integer.parseInt(part2), 'g', 1);
                        } else {
                            entra2 = txt[i][j - 1].getText();
                            parts2 = entra2.split(",");
                            part3 = parts2[0];
                            part4 = parts2[1];
                            a1.Añadir(part1.charAt(0), Integer.parseInt(part2), part3.charAt(0), 0);
                        }

                    } else if (i == 7) {
                        if (j == 0) {
                            a1.Añadir(part1.charAt(0), Integer.parseInt(part2), 'h', 1);
                        } else {
                            entra2 = txt[i][j - 1].getText();
                            parts2 = entra2.split(",");
                            part3 = parts2[0];
                            part4 = parts2[1];
                            a1.Añadir(part1.charAt(0), Integer.parseInt(part2), part3.charAt(0), 0);
                        }

                    } else if (i == 8) {
                        if (j == 0) {
                            a1.Añadir(part1.charAt(0), Integer.parseInt(part2), 'i', 1);
                        } else {
                            entra2 = txt[i][j - 1].getText();
                            parts2 = entra2.split(",");
                            part3 = parts2[0];
                            part4 = parts2[1];
                            a1.Añadir(part1.charAt(0), Integer.parseInt(part2), part3.charAt(0), 0);
                        }

                    } else if (i == 9) {
                        if (j == 0) {
                            a1.Añadir(part1.charAt(0), Integer.parseInt(part2), 'j', 1);
                        } else {
                            entra2 = txt[i][j - 1].getText();
                            parts2 = entra2.split(",");
                            part3 = parts2[0];
                            part4 = parts2[1];
                            a1.Añadir(part1.charAt(0), Integer.parseInt(part2), part3.charAt(0), 0);
                        }

                    } else if (i == 10) {
                        if (j == 0) {
                            a1.Añadir(part1.charAt(0), Integer.parseInt(part2), 'k', 1);
                        } else {
                            entra2 = txt[i][j - 1].getText();
                            parts2 = entra2.split(",");
                            part3 = parts2[0];
                            part4 = parts2[1];
                            a1.Añadir(part1.charAt(0), Integer.parseInt(part2), part3.charAt(0), 0);
                        }

                    } else if (i == 11) {
                        if (j == 0) {
                            a1.Añadir(part1.charAt(0), Integer.parseInt(part2), 'l', 1);
                        } else {
                            entra2 = txt[i][j - 1].getText();
                            parts2 = entra2.split(",");
                            part3 = parts2[0];
                            part4 = parts2[1];
                            a1.Añadir(part1.charAt(0), Integer.parseInt(part2), part3.charAt(0), 0);
                        }

                    } else if (i == 12) {
                        if (j == 0) {
                            a1.Añadir(part1.charAt(0), Integer.parseInt(part2), 'm', 1);
                        } else {
                            entra2 = txt[i][j - 1].getText();
                            parts2 = entra2.split(",");
                            part3 = parts2[0];
                            part4 = parts2[1];
                            a1.Añadir(part1.charAt(0), Integer.parseInt(part2), part3.charAt(0), 0);
                        }

                    } else if (i == 13) {
                        if (j == 0) {
                            a1.Añadir(part1.charAt(0), Integer.parseInt(part2), 'n', 1);
                        } else {
                            entra2 = txt[i][j - 1].getText();
                            parts2 = entra2.split(",");
                            part3 = parts2[0];
                            part4 = parts2[1];
                            a1.Añadir(part1.charAt(0), Integer.parseInt(part2), part3.charAt(0), 0);
                        }

                    } else if (i == 14) {
                        if (j == 0) {
                            a1.Añadir(part1.charAt(0), Integer.parseInt(part2), 'o', 1);
                        } else {
                            entra2 = txt[i][j - 1].getText();
                            parts2 = entra2.split(",");
                            part3 = parts2[0];
                            part4 = parts2[1];
                            a1.Añadir(part1.charAt(0), Integer.parseInt(part2), part3.charAt(0), 0);
                        }

                    } else if (i == 15) {
                        if (j == 0) {
                            a1.Añadir(part1.charAt(0), Integer.parseInt(part2), 'p', 1);
                        } else {
                            entra2 = txt[i][j - 1].getText();
                            parts2 = entra2.split(",");
                            part3 = parts2[0];
                            part4 = parts2[1];
                            a1.Añadir(part1.charAt(0), Integer.parseInt(part2), part3.charAt(0), 0);
                        }

                    } else if (i == 16) {
                        if (j == 0) {
                            a1.Añadir(part1.charAt(0), Integer.parseInt(part2), 'q', 1);
                        } else {
                            entra2 = txt[i][j - 1].getText();
                            parts2 = entra2.split(",");
                            part3 = parts2[0];
                            part4 = parts2[1];
                            a1.Añadir(part1.charAt(0), Integer.parseInt(part2), part3.charAt(0), 0);
                        }

                    } else if (i == 17) {
                        if (j == 0) {
                            a1.Añadir(part1.charAt(0), Integer.parseInt(part2), 'r', 1);
                        } else {
                            entra2 = txt[i][j - 1].getText();
                            parts2 = entra2.split(",");
                            part3 = parts2[0];
                            part4 = parts2[1];
                            a1.Añadir(part1.charAt(0), Integer.parseInt(part2), part3.charAt(0), 0);
                        }

                    } else if (i == 18) {
                        if (j == 0) {
                            a1.Añadir(part1.charAt(0), Integer.parseInt(part2), 's', 1);
                        } else {
                            entra2 = txt[i][j - 1].getText();
                            parts2 = entra2.split(",");
                            part3 = parts2[0];
                            part4 = parts2[1];
                            a1.Añadir(part1.charAt(0), Integer.parseInt(part2), part3.charAt(0), 0);
                        }

                    } else if (i == 19) {
                        if (j == 0) {
                            a1.Añadir(part1.charAt(0), Integer.parseInt(part2), 't', 1);
                        } else {
                            entra2 = txt[i][j - 1].getText();
                            parts2 = entra2.split(",");
                            part3 = parts2[0];
                            part4 = parts2[1];
                            a1.Añadir(part1.charAt(0), Integer.parseInt(part2), part3.charAt(0), 0);
                        }

                    } else if (i == 20) {
                        if (j == 0) {
                            a1.Añadir(part1.charAt(0), Integer.parseInt(part2), 'u', 1);
                        } else {
                            entra2 = txt[i][j - 1].getText();
                            parts2 = entra2.split(",");
                            part3 = parts2[0];
                            part4 = parts2[1];
                            a1.Añadir(part1.charAt(0), Integer.parseInt(part2), part3.charAt(0), 0);
                        }

                    } else if (i == 21) {
                        if (j == 0) {
                            a1.Añadir(part1.charAt(0), Integer.parseInt(part2), 'v', 1);
                        } else {
                            entra2 = txt[i][j - 1].getText();
                            parts2 = entra2.split(",");
                            part3 = parts2[0];
                            part4 = parts2[1];
                            a1.Añadir(part1.charAt(0), Integer.parseInt(part2), part3.charAt(0), 0);
                        }

                    } else if (i == 22) {
                        if (j == 0) {
                            a1.Añadir(part1.charAt(0), Integer.parseInt(part2), 'w', 1);
                        } else {
                            entra2 = txt[i][j - 1].getText();
                            parts2 = entra2.split(",");
                            part3 = parts2[0];
                            part4 = parts2[1];
                            a1.Añadir(part1.charAt(0), Integer.parseInt(part2), part3.charAt(0), 0);
                        }

                    } else if (i == 23) {
                        if (j == 0) {
                            a1.Añadir(part1.charAt(0), Integer.parseInt(part2), 'x', 1);
                        } else {
                            entra2 = txt[i][j - 1].getText();
                            parts2 = entra2.split(",");
                            part3 = parts2[0];
                            part4 = parts2[1];
                            a1.Añadir(part1.charAt(0), Integer.parseInt(part2), part3.charAt(0), 0);
                        }

                    } else if (i == 24) {
                        if (j == 0) {
                            a1.Añadir(part1.charAt(0), Integer.parseInt(part2), 'y', 1);
                        } else {
                            entra2 = txt[i][j - 1].getText();
                            parts2 = entra2.split(",");
                            part3 = parts2[0];
                            part4 = parts2[1];
                            a1.Añadir(part1.charAt(0), Integer.parseInt(part2), part3.charAt(0), 0);
                        }

                    } else if (i == 25) {
                        if (j == 0) {
                            a1.Añadir(part1.charAt(0), Integer.parseInt(part2), 'z', 1);
                        } else {
                            entra2 = txt[i][j - 1].getText();
                            parts2 = entra2.split(",");
                            part3 = parts2[0];
                            part4 = parts2[1];
                            a1.Añadir(part1.charAt(0), Integer.parseInt(part2), part3.charAt(0), 0);
                        }

                    }
                }else{
                    letras[i][j] = ' ';
                    valor[i][j] = 0;
                }
            }
        }
        
        for (int i = 0; i < 26; i++) {
            for (int j = 0; j < 9; j++) {
                System.out.println(letras[i][j]);
                System.out.println(valor[i][j]);
            }
        }
        a1.distanciaMinima(letras,valor);
        a1.Asignar(letras, valor);
        String impresion = a1.getDatos();
        labelMin.setText("La distancia mas corta es: " + impresion);
        a1.graficar(impresion);
        labelMin.setForeground(Color.red);
        
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        enviar();
    }

}

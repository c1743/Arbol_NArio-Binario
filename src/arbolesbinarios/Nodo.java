package arbolesbinarios;
/**
 *
 * @author AndresFWilT
 */
public class Nodo {

    private char dato;
    private int valor;

    private Nodo izquierdo, derecho;

    public Nodo(char dato, int valor) {
        this.dato = dato;
        this.valor = valor;
        this.izquierdo = null;
        this.derecho = null;
    }

    public char getDato() {
        return dato;
    }

    public void setDato(char dato) {
        this.dato = dato;
    }

    public int getValor() {
        return valor;
    }

    public void setValor(int valor) {
        this.valor = valor;
    }

    public Nodo getIzquierdo() {
        return izquierdo;
    }

    public void setIzquierdo(Nodo izquierdo) {
        this.izquierdo = izquierdo;
    }

    public Nodo getDerecho() {
        return derecho;
    }

    public void setDerecho(Nodo derecho) {
        this.derecho = derecho;
    }

}
